#ifndef _IDE_H_
#define _IDE_H_

#include "device.h"
#include "refs.h"

class IDE : public BlockDevice {
public:
    IDE(uint32_t id);

    /* From BlockIO */
    virtual StrongPtr<BlockBuffer> readBlock(size_t blockNumber);

    static void init(void);

    static StrongPtr<BlockDevice> a();
    static StrongPtr<BlockDevice> b();
    static StrongPtr<BlockDevice> c();
    static StrongPtr<BlockDevice> d();

};

#endif
