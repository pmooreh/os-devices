#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "ide.h"
#include "ide_support.h"

void kernelMain(void) {

    Debug::say("hello");

    StrongPtr<BlockDevice> c = IDE::c();

    StrongPtr<BlockBuffer> buffer = c->readBlock(0);
    buffer->ready->wait();

    for (int i=0; i<10; i++) {
        Debug::say("%x",(uint8_t)buffer->data[i]);
    }
    
    Debug::say("back");
 
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
    ideStats();
}
