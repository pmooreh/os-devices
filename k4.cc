#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "ide.h"
#include "ide_support.h"
#include "bcache.h"

void kernelMain(void) {

    Debug::say("hello");

    BufferCache *cache = new BufferCache();

    for (int i=0; i<4; i++) {
        StrongPtr<BlockDevice> dd = IDE::d();
        Debug::say("read0");
        StrongPtr<BlockBuffer> b0 = cache->readBlock(dd,0);
        Debug::say("read1");
        StrongPtr<BlockBuffer> b1 = cache->readBlock(dd,1);
        Debug::say("read2");
        StrongPtr<BlockBuffer> b2 = cache->readBlock(dd,2);
        Debug::say("read3");
        StrongPtr<BlockBuffer> b3 = cache->readBlock(dd,3);

        b3->ready->wait();
        Debug::say("done3");
        b2->ready->wait();
        Debug::say("done2");
        b1->ready->wait();
        Debug::say("done1");
        b0->ready->wait();
        Debug::say("done0");

        Debug::say("b0 %x",(uint8_t) b0->data[i+0]);
        Debug::say("b1 %x",(uint8_t) b1->data[i+1]);
        Debug::say("b2 %x",(uint8_t) b2->data[i+6]);
        Debug::say("b4 %x",(uint8_t) b3->data[i+3]);
    }
    
    Debug::say("done");
 
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
    ideStats();
}
