#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "ide.h"
#include "ide_support.h"

void kernelMain(void) {

    Debug::say("hello");

    for (int i=0; i<4; i++) {
        StrongPtr<BlockDevice> d = IDE::d();
        Debug::say("read0");
        StrongPtr<BlockBuffer> b0 = d->readBlock(0);
        Debug::say("read1");
        StrongPtr<BlockBuffer> b1 = d->readBlock(1);
        Debug::say("read2");
        StrongPtr<BlockBuffer> b2 = d->readBlock(2);
        Debug::say("read3");
        StrongPtr<BlockBuffer> b3 = d->readBlock(3);

        b3->ready->wait();
        Debug::say("done3");
        b2->ready->wait();
        Debug::say("done2");
        b1->ready->wait();
        Debug::say("done1");
        b0->ready->wait();
        Debug::say("done0");

        Debug::say("b0 %x",(uint8_t) b0->data[i+0]);
        Debug::say("b1 %x",(uint8_t) b1->data[i+1]);
        Debug::say("b2 %x",(uint8_t) b2->data[i+6]);
        Debug::say("b4 %x",(uint8_t) b3->data[i+3]);
    }
    
    Debug::say("done");
 
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
    ideStats();
}
