#ifndef _ALARM_H_
#define _ALARM_H_

#include "locks.h"
#include "stdint.h"
#include "refs.h"

class Alarm {

public:

    // initilize alarm sub-system
    static void init(void);

    // schedule an alarm in "after" seconds
    static void schedule(uint32_t after, StrongPtr<Event> event);

};

#endif
