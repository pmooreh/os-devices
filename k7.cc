#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "ide.h"
#include "ide_support.h"
#include "bcache.h"
#include "thread.h"
#include "stdint.h"

volatile int n=0;

void loop(StrongPtr<BufferCache> cache) {

    auto c = IDE::c();
    auto d = IDE::d();

    for (int i=0; i<100; i++) {
        StrongPtr<BlockBuffer> bx = cache->readBlock(c,0);
        StrongPtr<BlockBuffer> b0 = cache->readBlock(d,0);
        StrongPtr<BlockBuffer> b1 = cache->readBlock(d,1);
        StrongPtr<BlockBuffer> b2 = cache->readBlock(d,2);
        StrongPtr<BlockBuffer> b3 = cache->readBlock(d,3);

        b3->ready->wait(); getThenIncrement(&n,1);
        b2->ready->wait(); getThenIncrement(&n,1);
        b1->ready->wait(); getThenIncrement(&n,1);
        b0->ready->wait(); getThenIncrement(&n,1);
        bx->ready->wait(); getThenIncrement(&n,1);

        uint8_t v = bx->data[0];
        uint8_t u = 0xfa;
        if (v != u) {
            Debug::say("bad value u:%x v:%x",u,v);
            return;
        }

        v = b0->data[0];
        u = '1';
        if (v != u) {
            Debug::say("bad value u:%x v:%x",u,v);
            return;
        }

    }
    
}

void kernelMain(void) {
    StrongPtr<BufferCache> cache { new BufferCache() };

    StrongPtr<Thread> t0 = threadCreate(loop,cache);
    StrongPtr<Thread> t1 = threadCreate(loop,cache);
    StrongPtr<Thread> t2 = threadCreate(loop,cache);
    StrongPtr<Thread> t3 = threadCreate(loop,cache);

    t0->exitEvent->wait();
    t1->exitEvent->wait();
    t2->exitEvent->wait();
    t3->exitEvent->wait();

    Debug::say("done %d",n);
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
    ideStats();
}
