#include "device.h"

/* will use the readblock convenience method */


/* read up to nbyte starting at offset, never crosses block bounderies */
/* returns the number of bytes actually read */
size_t BlockIO::read(size_t offset, void* buf, size_t nbyte) {
	// calculate block
	size_t blockNum = offset / BLOCK_SIZE;
	size_t actuallyRead = BLOCK_SIZE - (offset % BLOCK_SIZE);

	if (actuallyRead > nbyte)
		actuallyRead = nbyte;

    // read the whole danged block, then wait on it
	StrongPtr<BlockBuffer> intermediate = readBlock(blockNum);
	intermediate->ready->wait();

	// Debug::printf("offset: %d\n", offset);
	// Debug::printf("nbyte: %d\n", nbyte);
	// Debug::printf("blockNum: %d\n", blockNum);
	// Debug::printf("actuallyRead: %d\n", actuallyRead);

	// so now it's been read in to intermediate. from here, step through
	// the data and transcribe

	//memcpy(buf, intermediate->data + (offset % BLOCK_SIZE) / 4, actuallyRead);

	for (uint32_t i = 0; i < actuallyRead; i++) {
		((char*)buf)[i] = intermediate->data[i + (offset % BLOCK_SIZE)];
	}

    return actuallyRead;
}

/* read nbyte starting at offset */
/* returns nbyte unless an error occured */
size_t BlockIO::readAll(size_t offset, void* buf, size_t nbyte) {
    // first, calculate number of blocks that will be covered
    uint32_t startBlock =  offset / BLOCK_SIZE;
    uint32_t endBlock = (offset + nbyte) / BLOCK_SIZE;
    uint32_t bufIndex = 0;

	Debug::printf("\n");
	Debug::printf("offset: %d\n", offset);
	Debug::printf("nbyte: %d\n", nbyte);
	Debug::printf("startBlock: %d\n", startBlock);
	Debug::printf("endBlock: %d\n", endBlock);


    // now loop through each block
    // read it in, then transcribe it into buf
    for (uint32_t currBlock = startBlock; currBlock <= endBlock; currBlock++) {
    	// read in block

    	StrongPtr<BlockBuffer> intermediate = readBlock(currBlock);
		intermediate->ready->wait();


		// now transcribe
		for (; (bufIndex + offset % BLOCK_SIZE) < BLOCK_SIZE; bufIndex++) {
			Debug::printf("bufIndex: %d\n", bufIndex);
			((char*)buf)[bufIndex] = intermediate->data[(bufIndex % BLOCK_SIZE)];
		}
    }

    return nbyte;
}
