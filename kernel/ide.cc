#include "ide.h"
#include "ide_support.h"
#include "machine.h"
#include "stdint.h"
#include "idt.h"
#include "pic.h"

#include "locks.h"
#include "thread.h"

/*****************/
/* The IDE class */
/*****************/

// internal device manager struct
struct IdeInfo {
    StrongPtr<BlockDevice> ptrA;
    StrongPtr<BlockDevice> ptrB;
    StrongPtr<BlockDevice> ptrC;
    StrongPtr<BlockDevice> ptrD;
    // throw a lock in the mix
    BlockingLock* lock;
};
// instance's info struct
static IdeInfo* ideInfo = nullptr;


StrongPtr<BlockDevice> IDE::a() {   
    return ideInfo->ptrA;
}
    
StrongPtr<BlockDevice> IDE::b() {   
    return ideInfo->ptrB;
}
    
StrongPtr<BlockDevice> IDE::c() {   
    return ideInfo->ptrC;
}
    
StrongPtr<BlockDevice> IDE::d() {  
    return ideInfo->ptrD;
}
    
void IDE::init() {
    // carve out space for IDE's
    ideInfo = new IdeInfo();

    // initialize all the drive
    ideInfo->ptrA = StrongPtr<BlockDevice>(new IDE(0));
    ideInfo->ptrB = StrongPtr<BlockDevice>(new IDE(1));
    ideInfo->ptrC = StrongPtr<BlockDevice>(new IDE(2));
    ideInfo->ptrD = StrongPtr<BlockDevice>(new IDE(3));

    // initialize lock
    ideInfo->lock = new BlockingLock();
}


IDE::IDE(uint32_t drive) : BlockDevice(drive) {
}


void threadRead(StrongPtr<BlockBuffer> buffer) {
    // read the file
    // 1 block = 8 sectors
    ideInfo->lock->lock();
    uint32_t sector = buffer->blockNum * 8;

    for (uint32_t i = 0; i < 8; i++) {
        
        readSector(buffer->id, sector + i, (uint32_t*)(buffer->data + i * 512));
    }
    ideInfo->lock->unlock();
    // once finished, signal the done event
    buffer->ready->signal();
}

StrongPtr<BlockBuffer> IDE::readBlock(size_t blockNum) {

    // make my block buffer
    // Debug::printf("    1 IN READBLOCK\n");
    // Debug::printf("    blockNum: %d\n", blockNum);
    // Debug::printf("    id: %d\n", id);
    StrongPtr<BlockBuffer> buffer = StrongPtr<BlockBuffer>(new BlockBuffer(id, blockNum));
    // Debug::printf("    2 IN READBLOCK\n");

    threadCreate(threadRead, buffer);

    return buffer;
}
