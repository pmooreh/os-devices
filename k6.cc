#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "ide.h"
#include "ide_support.h"
#include "bcache.h"
#include "thread.h"

volatile int n=0;

void loop(StrongPtr<CachedDevice> dev) {

    for (int i=0; i<100; i++) {
        StrongPtr<BlockBuffer> b0 = dev->readBlock(0);
        StrongPtr<BlockBuffer> b1 = dev->readBlock(1);
        StrongPtr<BlockBuffer> b2 = dev->readBlock(2);
        StrongPtr<BlockBuffer> b3 = dev->readBlock(3);

        b3->ready->wait(); getThenIncrement(&n,1);
        b2->ready->wait(); getThenIncrement(&n,1);
        b1->ready->wait(); getThenIncrement(&n,1);
        b0->ready->wait(); getThenIncrement(&n,1);

    }
    
}

void kernelMain(void) {
    StrongPtr<BufferCache> cache { new BufferCache() };
    StrongPtr<BlockDevice> dd = IDE::d();
    StrongPtr<CachedDevice> cd { new CachedDevice(cache,dd) };

    StrongPtr<Thread> t0 = threadCreate(loop,cd);
    StrongPtr<Thread> t1 = threadCreate(loop,cd);
    StrongPtr<Thread> t2 = threadCreate(loop,cd);
    StrongPtr<Thread> t3 = threadCreate(loop,cd);

    t0->exitEvent->wait();
    t1->exitEvent->wait();
    t2->exitEvent->wait();
    t3->exitEvent->wait();

    Debug::say("done %d",n);
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
    ideStats();
}
