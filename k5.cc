#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "ide.h"
#include "ide_support.h"
#include "bcache.h"

void kernelMain(void) {

    Debug::say("hello");

    BufferCache *cache = new BufferCache();

    int n=0;

    for (int i=0; i<1000; i++) {
        StrongPtr<BlockDevice> dd = IDE::d();
        StrongPtr<BlockBuffer> b0 = cache->readBlock(dd,0);
        StrongPtr<BlockBuffer> b1 = cache->readBlock(dd,1);
        StrongPtr<BlockBuffer> b2 = cache->readBlock(dd,2);
        StrongPtr<BlockBuffer> b3 = cache->readBlock(dd,3);

        b3->ready->wait(); n++;
        b2->ready->wait(); n++;
        b1->ready->wait(); n++;
        b0->ready->wait(); n++;

    }
    
    Debug::say("done %d",n);
 
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
    ideStats();
}
