#include "pic.h"
#include "stdint.h"
#include "machine.h"
#include "debug.h"
#include "idt.h"

#define C1 0x20           /* command port for PIC1 */
#define D1 (C1 + 1)       /* data port for PIC1 */
#define C2 0xA0           /* command port for PIC2 */
#define D2 (C2 + 1)       /* data port for PIC2 */

uint32_t picBase = 32;

void picInit(void) {
    /* ICW1 */
    outb(C1,0x11);        /* init with ICW4, not single */
    outb(C2,0x11);        /* init with ICW4, not single */

    /* ICW2 */
    outb(D1,picBase);    /* IDT index for IRQ0 */
    outb(D2,picBase+8);  /* IDT index for IRQ8 */

    /* ICW3 */
    outb(D1, 1 << 2);     /* tells master that the save is at IRQ2 */
    outb(D2, 2);          /* tells salve that it's connected at IRQ2 */

    /* ICW4 */
    outb(D1, 1);          /* 8086 mode */
    outb(D2, 1);          /* 8086 mode */

    /* enable all */
    outb(D1,0);
    outb(D2,0);

    idtAdd(picBase+0,(uint32_t) pic0);
    idtAdd(picBase+1,(uint32_t) pic1);
    idtAdd(picBase+2,(uint32_t) pic2);
    idtAdd(picBase+3,(uint32_t) pic3);
    idtAdd(picBase+4,(uint32_t) pic4);
    idtAdd(picBase+5,(uint32_t) pic5);
    idtAdd(picBase+6,(uint32_t) pic6);
    idtAdd(picBase+7,(uint32_t) pic7);
    idtAdd(picBase+8,(uint32_t) pic8);
    idtAdd(picBase+9,(uint32_t) pic9);
    idtAdd(picBase+10,(uint32_t) pic10);
    idtAdd(picBase+11,(uint32_t) pic11);
    idtAdd(picBase+12,(uint32_t) pic12);
    idtAdd(picBase+13,(uint32_t) pic13);
    idtAdd(picBase+14,(uint32_t) pic14);
    idtAdd(picBase+15,(uint32_t) pic15);
}

extern "C" void picHandler(int num) {
    Debug::printf("interrupt #%d\n",num);
    picEoi(num);
}

void picEnable() {
    sti();
}
    
/* Disable interrupts, return true if they were enabled */
bool picDisable() {
    return (cli() & 0x200) == 0x200;
}

void picRestore(bool wasEnabled) {
    if (wasEnabled) {
        picEnable();
    } else {
        picDisable();
    }
}
    

void picEoi(uint32_t irq) {
    if (irq >= 8) {
        /* let PIC2 know */
        outb(C2,0x20);
    }
    /* we always let PIC1 know because PIC2 is routed though PIC1 */
    outb(C1,0x20);
}
