#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "ide.h"
#include "ide_support.h"

void kernelMain(void) {

    Debug::say("hello");

    {
        StrongPtr<BlockDevice> c = IDE::c();

        uint8_t ch;
        size_t n = c->read(0,&ch,sizeof(char));
        Debug::say("n = %d",n);
        Debug::say("c = %x",ch);
    }

    {
        StrongPtr<BlockDevice> d = IDE::d();
        char data[8];
        size_t n = d->read(4095,data,7);
        Debug::say("n = %d",n);
        n = d->read(4096,&data[1],6);
        Debug::say("n = %d",n);
        data[7] = 0;
        Debug::say("%s",data);
    }
    
    {
        StrongPtr<BlockDevice> d = IDE::d();
        char data[8];
        size_t n = d->readAll(4095,data,7);
        Debug::say("n = %d",n);
        data[7] = 0;
        Debug::say("%s",data);
    }

    Debug::say("done");
 
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
    ideStats();
}
