#include "bcache.h"

StrongPtr<BlockBuffer> BufferCache::readBlock(StrongPtr<BlockDevice> dev,
    size_t blockNum)
{
    // search through the queue, look to see if this block has already been read
    lock->lock();
    StrongPtr<BlockBuffer> start = cacheQueue->remove();

    if (!start.isNull()) {

    	if (start->blockNum == blockNum && start->id == dev->id) {
			cacheQueue->add(start);
			lock->unlock();
			return start;
		}

    	// search through
    	cacheQueue->add(start);
    	StrongPtr<BlockBuffer> current = cacheQueue->remove();

    	while (current != start) {
    		if (current->blockNum == blockNum && current->id == dev->id) {
    			cacheQueue->add(current);
    			lock->unlock();
    			return current;
    		}
    		else {
    			cacheQueue->add(current);
    			current = cacheQueue->remove();
    		}
    	}

    	cacheQueue->add(current);
    }

    // if not, read and add to queue
	StrongPtr<BlockBuffer> newBlock = dev->readBlock(blockNum);
	cacheQueue->add(newBlock);
	lock->unlock();
	return newBlock;

}
