#ifndef _BCACHE_H_
#define _BCACHE_H_

#include "stdint.h"
#include "refs.h"
#include "device.h"
#include "queue.h"
#include "locks.h"

class BufferCache {

    // have a queue to search through for each device?
    StrongQueue<BlockBuffer>* cacheQueue;
    BlockingLock* lock;

public:
    
    BufferCache() : cacheQueue(new StrongQueue<BlockBuffer>()), lock(new BlockingLock()) {}

    StrongPtr<BlockBuffer> readBlock(StrongPtr<BlockDevice> dev,
        size_t blockNum);

};

class CachedDevice : public BlockIO {
    StrongPtr<BufferCache> cache;
    StrongPtr<BlockDevice> device;
public:
    CachedDevice(StrongPtr<BufferCache> cache,
        StrongPtr<BlockDevice> device) : cache(cache), device(device)
    {
    }

    virtual StrongPtr<BlockBuffer> readBlock(size_t blockNum) {
        return cache->readBlock(device,blockNum);
    }
};

#endif
